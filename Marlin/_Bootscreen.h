/**
 * Marlin 3D Printer Firmware
 * Copyright (c) 2020 MarlinFirmware [https://github.com/MarlinFirmware/Marlin]
 *
 * Based on Sprinter and grbl.
 * Copyright (c) 2011 Camiel Gubbels / Erik van der Zalm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

/**
 * Custom Boot Screen bitmap
 *
 * Place this file in the root with your configuration files
 * and enable SHOW_CUSTOM_BOOTSCREEN in Configuration.h.
 *
 * Use the Marlin Bitmap Converter to make your own:
 * http://marlinfw.org/tools/u8glib/converter.html
 */

#define CUSTOM_BOOTSCREEN_TIMEOUT 5000
#define CUSTOM_BOOTSCREEN_BMPWIDTH 128
#define CUSTOM_BOOTSCREEN_INVERTED

const unsigned char custom_start_bmp[] PROGMEM = {
0x1F, 0xFF, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x0F, 0xFF, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x07, 0xFF, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x03, 0xFF, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x01, 0xFF, 0x00, 0x01, 0xFF, 0xC1, 0xFE, 0x00, 0x1F, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0xFF, 0x00, 0x01, 0xFF, 0x00, 0x7E, 0x00, 0x1F, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x7F, 0x00, 0x01, 0xFE, 0x3E, 0x3F, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x3F, 0x00, 0x01, 0xFE, 0x7F, 0x3F, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x1F, 0x00, 0x01, 0xFE, 0x7F, 0xFF, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x0F, 0x00, 0x01, 0xFE, 0x3F, 0xFF, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x07, 0x00, 0x01, 0xFF, 0x03, 0xFF, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x03, 0x00, 0x01, 0xFF, 0xC0, 0xFF, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x01, 0x00, 0x01, 0xFF, 0xFC, 0x7F, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0x3F, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFC, 0xFF, 0x3F, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x3F, 0xFF, 0x40, 0x01, 0xFC, 0x7F, 0x3F, 0xF3, 0xFF, 0x1F, 0x8F, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x1F, 0xFF, 0x60, 0x01, 0xFE, 0x3E, 0x3F, 0xF3, 0xFF, 0x8F, 0x1F, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x0F, 0xFF, 0x70, 0x01, 0xFF, 0x00, 0x7F, 0xF3, 0xFF, 0xC0, 0x3F, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x07, 0xFF, 0x78, 0x01, 0xFF, 0x81, 0xFF, 0xF3, 0xFF, 0xE0, 0x7F, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x03, 0xFF, 0x7C, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x01, 0xFF, 0x7E, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0xFF, 0x7F, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x7F, 0x7F, 0x81, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x3F, 0x7F, 0xC1, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x1F, 0x7F, 0xE1, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x0F, 0x7F, 0xF1, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x07, 0x7F, 0xF9, 0xFE, 0x01, 0xFF, 0xF3, 0xFF, 0xE0, 0x7F, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x03, 0x7F, 0xFD, 0xFE, 0x00, 0x7F, 0xF3, 0xFF, 0xC0, 0x3F, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x01, 0x7F, 0xFD, 0xFE, 0x7E, 0x3F, 0xF3, 0xFF, 0x8F, 0x1F, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x3F, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x9F, 0xF3, 0xFE, 0x3F, 0xC7, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x9F, 0xF3, 0xFE, 0x7F, 0xE7, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x9F, 0xF3, 0xFE, 0x7F, 0xE7, 0xFD, 0xFF, 0xFA, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x9F, 0xF3, 0xFE, 0x7F, 0xE7, 0xFC, 0xFF, 0xFB, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x9F, 0xF3, 0xFE, 0x7F, 0xE7, 0xFC, 0x7F, 0xFB, 0x80, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x9F, 0xF3, 0xFE, 0x7F, 0xE7, 0xFC, 0x3F, 0xFB, 0xC0, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x9F, 0xF3, 0xFE, 0x3F, 0xC7, 0xFC, 0x1F, 0xFB, 0xE0, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7F, 0x3F, 0xF3, 0xFF, 0x3F, 0xCF, 0xFC, 0x0F, 0xFB, 0xF0, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x7E, 0x3F, 0xF3, 0xFF, 0x8F, 0x1F, 0xFC, 0x07, 0xFB, 0xF8, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x00, 0x7F, 0xF3, 0xFF, 0xC0, 0x3F, 0xFC, 0x03, 0xFB, 0xFC, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFE, 0x01, 0xFF, 0xF3, 0xFF, 0xE0, 0x7F, 0xFC, 0x01, 0xFB, 0xFE, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0xFB, 0xFF, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x7B, 0xFF, 0x80,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x3B, 0xFF, 0xC0,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x1B, 0xFF, 0xE0,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x0B, 0xFF, 0xF0,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x0B, 0xFF, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xF8, 0x78, 0xEF, 0x7D, 0xC7, 0xC7, 0x7B, 0xFC, 0x00, 0x0D, 0xFF, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFB, 0xB7, 0x6F, 0xBB, 0xBB, 0xBB, 0x3B, 0xFC, 0x00, 0x0E, 0xFF, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFB, 0xAF, 0xAF, 0xBB, 0x7D, 0x7D, 0x5B, 0xFC, 0x00, 0x0F, 0x7F, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFB, 0xAF, 0xAF, 0xD7, 0x7F, 0x7D, 0x5B, 0xFC, 0x00, 0x0F, 0xBF, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xF8, 0x6F, 0xAF, 0xEF, 0x71, 0x7D, 0x6B, 0xFC, 0x00, 0x0F, 0xDF, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFB, 0xEF, 0xAF, 0xEF, 0x7D, 0x7D, 0x6B, 0xFC, 0x00, 0x0F, 0xEF, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFB, 0xF7, 0x6F, 0xEF, 0xBB, 0xBB, 0x73, 0xFC, 0x00, 0x0F, 0xF7, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFB, 0xF8, 0xE0, 0xEF, 0xC7, 0xC7, 0x7B, 0xFC, 0x00, 0x0F, 0xFB, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x0F, 0xFD, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x0F, 0xFE, 0xF8,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x0F, 0xFF, 0x78,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x0F, 0xFF, 0xB8,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x0F, 0xFF, 0xD8,
0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x0F, 0xFF, 0xE8,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
